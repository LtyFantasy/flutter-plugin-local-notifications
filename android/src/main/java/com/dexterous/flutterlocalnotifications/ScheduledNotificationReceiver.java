package com.dexterous.flutterlocalnotifications;

import android.app.ActivityManager;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationManagerCompat;

import com.dexterous.flutterlocalnotifications.models.NotificationDetails;
import com.dexterous.flutterlocalnotifications.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by michaelbui on 24/3/18.
 */

public class ScheduledNotificationReceiver extends BroadcastReceiver {

    private static final String TAG = "ScheduledNotification";
    @Override
    public void onReceive(final Context context, Intent intent) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        String notificationDetailsJson = intent.getStringExtra(FlutterLocalNotificationsPlugin.NOTIFICATION_DETAILS);
        boolean repeat = intent.getBooleanExtra(FlutterLocalNotificationsPlugin.REPEAT, false);
        // TODO: remove this branching logic as it's legacy code to fix an issue where notifications weren't reporting the correct time
        if (StringUtils.isNullOrEmpty(notificationDetailsJson)) {
            Notification notification = intent.getParcelableExtra(FlutterLocalNotificationsPlugin.NOTIFICATION);
            notification.when = System.currentTimeMillis();
            int notificationId = intent.getIntExtra(FlutterLocalNotificationsPlugin.NOTIFICATION_ID,
                    0);
            notificationManager.notify(notificationId, notification);
            if (repeat) {
                return;
            }
            FlutterLocalNotificationsPlugin.removeNotificationFromCache(notificationId, context);
        } else {
            Gson gson = FlutterLocalNotificationsPlugin.buildGson();
            Type type = new TypeToken<NotificationDetails>() {
            }.getType();
            NotificationDetails notificationDetails = gson.fromJson(notificationDetailsJson, type);
            Log.d(FlutterLocalNotificationsPlugin.TAG, "context.getPackageName(): " + context.getPackageName());
            String foregroundActivity=getForegroundActivity(context);
            if (foregroundActivity!=null&&foregroundActivity.contains(context.getPackageName())) {
                FlutterLocalNotificationsPlugin.notificationOnActive(notificationDetails);
            } else {
                FlutterLocalNotificationsPlugin.showNotification(context, notificationDetails);
            }
            if (repeat) {
                return;
            }
            FlutterLocalNotificationsPlugin.removeNotificationFromCache(notificationDetails.id, context);
        }

    }


    public String getForegroundActivity(Context mContext) {
        ActivityManager mActivityManager =
                (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        if (mActivityManager.getRunningTasks(1) == null) {
            return null;
        }
        List<ActivityManager.RunningTaskInfo> tasks=mActivityManager.getRunningTasks(1);
        if(tasks.size()==0){
            return null;
        }
        ActivityManager.RunningTaskInfo mRunningTask =
                tasks.get(0);
        if (mRunningTask == null) {
            return null;
        }

        String pkgName = mRunningTask.topActivity.getPackageName();
        //String activityName =  mRunningTask.topActivity.getClassName();
        Log.d(FlutterLocalNotificationsPlugin.TAG, "pkgName: " + pkgName);
        return pkgName;
    }

}
